pipeline {
    agent any

    stages {
        stage('Checkout') {
            steps {
                // Checkout your code from version control
                git 'https://gitlab.com/h4mdi/mvn.git'
            }
        }

        stage('Build') {
            steps {
                script {
                    // Get the list of changed modules
                    def changedModules = getChangedModules()

                    // Build only the changed modules
                    for (module in changedModules) {
                        // Change directory to the module
                        dir(module) {
                            // Run Maven build
                            sh 'mvn clean install'
                        }
                    }
                }
            }
        }
    }
}

// Function to get list of changed modules
def getChangedModules() {
    def changedModules = []
    // Use Git to get the list of changed files
    def changedFiles = sh(returnStdout: true, script: 'git diff --name-only origin/master HEAD').trim()

    // Iterate through changed files to extract module names
    changedFiles.eachLine { file ->
        def module = file =~ /([^\/]+)/
        if (module) {
            changedModules.add(module[0][0])
        }
    }

    return changedModules.unique()
}
